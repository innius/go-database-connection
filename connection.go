package connection

import (
	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
)

// Open returns a new connection based on service secret value
func Open(conn Connection) (*sqlx.DB, error) {
	sql, err := sqlx.Open(conn.Engine(), conn.String())
	if err != nil {
		return nil, errors.Wrap(err, "could not open connection")
	}
	if err := sql.Ping(); err != nil {
		return nil, errors.Wrap(err, "could not Ping connection")
	}
	return sql, nil
}

type Connection interface {
	Engine() string
	String() string
}

