package internal

import (
	"bitbucket.org/innius/go-database-connection"
	"encoding/json"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/secretsmanager"
	"github.com/pkg/errors"
)

func NewConnection(secretArn string, sess *session.Session, v connection.Connection) error {
	ss, err := secretsmanager.New(sess).GetSecretValue(&secretsmanager.GetSecretValueInput{
		SecretId: aws.String(secretArn),
	})
	if err != nil {
		return errors.Wrap(err, "could not get mysql database secret")
	}

	err = json.Unmarshal([]byte(aws.StringValue(ss.SecretString)), v)
	if err != nil {
		return errors.Wrap(err, "could not unmarshal the secret")
	}
	return nil
}

type ConnectionProperties struct {
	UserName string `json:"username,omitempty"`
	Password string `json:"password,omitempty"`
	Engine   string `json:"engine,omitempty"`
	Host     string `json:"host,omitempty"`
	Port     string `json:"port,omitempty"`
	Database string `json:"dbname,omitempty"`
}
