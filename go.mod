module bitbucket.org/innius/go-database-connection

require (
	github.com/aws/aws-sdk-go v1.15.82
	github.com/jmoiron/sqlx v1.2.0
	github.com/lib/pq v1.10.2
	github.com/pkg/errors v0.8.0
)

require (
	github.com/jmespath/go-jmespath v0.0.0-20160202185014-0b12d6b521d8 // indirect
	github.com/stretchr/testify v1.3.0 // indirect
	golang.org/x/net v0.0.0-20190403144856-b630fd6fe46b // indirect
	google.golang.org/appengine v1.5.0 // indirect
)
