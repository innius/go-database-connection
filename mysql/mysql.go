package mysql

import (
	"fmt"

	"bitbucket.org/innius/go-database-connection/internal"
	"github.com/aws/aws-sdk-go/aws/session"
)

type mysqlConnection struct {
	*internal.ConnectionProperties
}

func (c *mysqlConnection) Engine() string {
	return c.ConnectionProperties.Engine
}

func (c *mysqlConnection) String() string {
	return fmt.Sprintf(
		"%s:%s@tcp(%s:%v)/%s?%s", c.UserName, c.Password, c.Host, c.Port, c.Database, "charset=utf8&parseTime=True&loc=UTC")
}

// convenience method for creating a db connection for a service
func New(secretArn string, sess *session.Session) (*mysqlConnection, error) {
	conn := mysqlConnection{}
	err := internal.NewConnection(secretArn, sess, &conn)
	return &conn, err
}
