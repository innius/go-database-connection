package redshift

import (
	"bitbucket.org/innius/go-database-connection/internal"
	"github.com/aws/aws-sdk-go/aws/session"
	"fmt"
	_ "github.com/lib/pq"
)

const Format = "2006-01-02 15:04:05"

type redshiftConnection struct {
	*internal.ConnectionProperties
}

func (c *redshiftConnection) String() string {
	return fmt.Sprintf("host=%v port=%v dbname=%v user=%v password=%v sslmode=disable connect_timeout=10",
		c.Host, c.Port, c.Database, c.UserName, c.Password)
}

func (c *redshiftConnection) Engine() string {
	return "postgres"
}

// convenience method for creating a db connection for a service
func New(secretArn string, sess *session.Session) (*redshiftConnection, error) {
	conn := redshiftConnection{}
	err := internal.NewConnection(secretArn, sess, &conn)
	return &conn, err
}
