package localreshift

import (
	"bitbucket.org/innius/go-database-connection/internal"
	"bitbucket.org/innius/go-database-connection/redshift"
	"fmt"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"github.com/pkg/errors"
	"time"
)

type LocalRedshift struct {
	*sqlx.DB
	*internal.ConnectionProperties
}

var connectionProperties = &internal.ConnectionProperties{
	Engine:   "postgres",
	Host:     "localhost",
	Port:     "5432",
	Database: "postgres",
	UserName: "postgres",
	Password: "secret",
}

func (c *LocalRedshift) String() string {
	return fmt.Sprintf("host=%v port=%v dbname=%v user=%v password=%v sslmode=disable connect_timeout=10",
		c.Host, c.Port, c.Database, c.UserName, c.Password)
}

func (c *LocalRedshift) Engine() string {
	return "postgres"
}

func NewLocalDatabase() (*LocalRedshift, error) {
	conn := &LocalRedshift{ConnectionProperties: connectionProperties}

	sql, err := sqlx.Open("postgres", conn.String())
	if err != nil {
		return nil, errors.Wrap(err, "could not open connection")
	}
	if err := sql.Ping(); err != nil {
		return nil, errors.Wrap(err, "could not Ping connection")
	}
	conn.DB = sql
	return conn, nil
}

type DataPoint struct {
	Mid       string
	Sid       string
	Timestamp time.Time
	Value     float64
}

func (db *LocalRedshift) InsertData(table string, values []DataPoint) error {
	if len(values) == 0 {
		return errors.New("No data")
	}

	query := fmt.Sprintf("INSERT INTO %s  (machine_id, sensor_id, event_timestamp, sensor_reading_value) VALUES ", table)
	for _, dp := range values {
		query += fmt.Sprintf("('%v', '%v', '%s', '%v'),", dp.Mid, dp.Sid, dp.Timestamp.Format(redshift.Format), dp.Value)
	}
	_, err := db.Exec(query[:len(query)-1]) //remove the pending comma
	return err
}
